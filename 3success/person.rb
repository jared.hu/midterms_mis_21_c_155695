class Person
	attr_accessor :name, :success_points
	def initialize(name, success_points)
		@name = name
		@success_points = success_points
		name = []
	end

	def to_s
		puts "@name"
	end

	def expected_score(another_person)
		return 1/(1+10^(another_person.success_points-@success_points)/400
	end

	def new_success_points(score, another_person)
		K = 32
		return @success_points + K * (score - expected_score(another_person))
	end

	def self.update_success_points(winner, loser)
		winner.new_success_points = 1
		loser.new_success_points = 0
	end
end