class ToDoList
def initialize()
 @pending_tasks = []
 @accomplished_tasks = []
end

def add_task(task_name)
    @pending_tasks << task_name
    puts "#{task_name} has been added to your task list!"
end

def view_pending_tasks
	index = 0
    puts "Pending tasks:"
    @pending_tasks.each do |a|
    	index += 1
        puts "(#{index}) #{a}"
    end
end

def accomplish_task(index)
    puts "#{@pending_tasks[index]} has been marked as accomplished."
    @accomplished_tasks << @pending_tasks[index]
    @pending_tasks.delete_at(index)
end

def view_accomplished_tasks
	index = 0
    puts "Accomplished tasks:"
    @accomplished_tasks.each do |a|
    	index += 1
        puts "(#{index}) #{a}"
    end
end
end