require 'date'
class Budget
	attr_accessor :balance, :expense, :income 
	def initialize (balance)
		puts "Initial balance: #{balance}"
		@balance = balance
		@expense = 0.0
		@income = 0.0	
	#expense
		@january_expense = 0.0
		@february_expense = 0.0 
		@march_expense = 0.0
		@april_expense = 0.0
		@may_expense = 0.0
		@june_expense = 0.0
		@july_expense = 0.0
		@august_expense = 0.0
		@september_expense = 0.0
		@october_expense = 0.0
		@november_expense = 0.0
		@december_expense = 0.0
	#income
		@january_income = 0.0
		@february_income = 0.0
		@march_income = 0.0
		@april_income = 0.0
		@may_income = 0.0
		@june_income = 0.0
		@july_income = 0.0		
		@august_income = 0.0
		@september_income = 0.0
		@october_income = 0.0
		@november_income = 0.0
		@december_income = 0.0
	end

	def add_expense (item, amount, date)
		@balance -= amount.to_f
		@expense += amount.to_f
		d = Date.parse(date)
		dates = date.split("-")
		if dates[1] == '06'
			@june_expense += amount.to_f
		elsif dates[1] == '07'
			@july_expense += amount.to_f
		end
		puts "#{item}: An amount of #{amount} was deducted from the balance on #{d.strftime("%B %d, %Y")}."
	end

	def add_income (item, amount, date)
		
		@balance += amount.to_f
		@income += amount.to_f
		d = Date.parse(date)
		dates = date.split("-")

		if dates[1] == '06'
			@june_income += amount.to_f
		elsif dates[1] == '07'
			@july_income += amount.to_f
		end
		puts "#{item}: An amount of #{amount} was added to the balance on #{d.strftime("%B %d, %Y")}."
	end

	def monthly_report (month, year)
		m=Date::MONTHNAMES[month]
		puts "========================================"
		puts "MONTHLY REPORT: #{m} #{year}"
		if month == 6
			puts "Total Expense: #{@june_expense}"
			puts "Total Income: #{@june_income}"
		elsif month==7
			puts "Total Expense: #{@july_expense}"
			puts "Total Income: #{@july_income}"
		end
		puts "======================================="
	end

	def current
		puts "Balance: #{@balance}"
	end
end